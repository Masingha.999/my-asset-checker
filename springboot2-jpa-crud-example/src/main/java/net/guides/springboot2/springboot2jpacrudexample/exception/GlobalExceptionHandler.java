package net.guides.springboot2.springboot2jpacrudexample.exception;

import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
public class GlobalExceptionHandler {
    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<?> resourseNotFoundException(ResourceNotFoundException ex, WebRequest request){
        Erroretails erroretails = new Erroretails(new Date(), ex.getMessage(), request.getDescription(false));
        return new ResponseEntity<>(erroretails, HttpStatus.NOT_FOUND);

    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> globleExceptionHandler (Exception ex, WebRequest request){
        Erroretails erroretails = new Erroretails(new Date(), ex.getMessage(), request.getDescription(false));
        return new ResponseEntity<>(erroretails,HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
