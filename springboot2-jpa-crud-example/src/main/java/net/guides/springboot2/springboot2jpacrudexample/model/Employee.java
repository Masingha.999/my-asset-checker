package net.guides.springboot2.springboot2jpacrudexample.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "employees")

public class Employee {

    private long id;
    private String f_Name;
    private String l_Name;
    private String email_Id;

    public Employee(){

    }

    public Employee(String f_Name, String l_Name, String email_Id) {
        this.f_Name = f_Name;
        this.l_Name = l_Name;
        this.email_Id = email_Id;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public long getId(){
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Column(name = "f_Name", nullable = false)
    public String getF_Name() {
        return f_Name;
    }

    public void setF_Name(String f_Name) {
        this.f_Name = f_Name;
    }

    @Column(name = "l_Name", nullable = false)
    public String getL_Name() {
        return l_Name;
    }

    public void setL_Name(String l_Name) {
        this.l_Name = l_Name;
    }

    @Column(name = "email_Id",nullable = false)
    public String getEmail_Id() {
        return email_Id;
    }

    public void setEmail_Id(String email_Id) {
        this.email_Id = email_Id;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", f_Name='" + f_Name + '\'' +
                ", l_Name='" + l_Name + '\'' +
                ", email_Id='" + email_Id + '\'' +
                '}';
    }
}
