package net.guides.springboot2.springboot2jpacrudexample.model;
import javax.persistence.*;

@Entity
@Table (name = "disc")

public class Disc {

    private long Disc_Id;
    private String Date;
    private String Note;
    private Double Price;
    private Employee employee;

    public Disc(){

    }

    public Disc(String Date, String Note, Double Price, Employee employee) {
        this. Date = Date;
        this. Note = Note;
        this. Price = Price;
        this.employee = employee;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public long getDisc_Id(){
        return Disc_Id;
    }

    public void setDisc_Id(long Disc_Id) {
        this.Disc_Id = Disc_Id;
    }

    @Column(name = "date",nullable = false)
    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
       this. Date = date;
    }

    @Column(name = "note", nullable = false)
    public String getNote() {
        return Note;
    }

    public void setNote(String note) {
        this.Note = note;
    }

    @Column(name = "price", nullable = false)
    public Double getPrice() {
        return Price;
    }

    public void setPrice(Double price) {
        this.Price = price;
    }

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id")
    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    @Override
    public String toString() {
        return "Disc{" +
                "Disc_Id=" + Disc_Id +
                ", Date='" + Date + '\'' +
                ", Note='" + Note + '\'' +
                ", Price=" + Price +
                '}';
    }
}
